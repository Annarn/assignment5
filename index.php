<?php 
/** The start page of the application. 
 * Control is forwarded to the controller
 */

    include_once("controller/controller.php");

	$controller = new Controller();
	$controller->invoke();

?>