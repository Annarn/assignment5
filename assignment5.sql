-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03. Nov, 2017 16:05 PM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `id` varchar(20) COLLATE utf8_danish_ci NOT NULL,
  `clubName` varchar(30) COLLATE utf8_danish_ci NOT NULL,
  `cityName` varchar(30) COLLATE utf8_danish_ci NOT NULL,
  `county` varchar(30) COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(10) COLLATE utf8_danish_ci NOT NULL,
  `firstName` varchar(20) COLLATE utf8_danish_ci NOT NULL,
  `lastName` varchar(20) COLLATE utf8_danish_ci NOT NULL,
  `yearOfBirth` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skierclubseason`
--

CREATE TABLE `skierclubseason` (
  `userName` varchar(10) COLLATE utf8_danish_ci NOT NULL,
  `fallYear` int(11) NOT NULL,
  `clubId` varchar(20) COLLATE utf8_danish_ci DEFAULT NULL,
  `totalDistance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`);

--
-- Indexes for table `skierclubseason`
--
ALTER TABLE `skierclubseason`
  ADD PRIMARY KEY (`fallYear`,`userName`),
  ADD KEY `FK_SCSClub` (`clubId`),
  ADD KEY `FK_SCSSkier` (`userName`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `skierclubseason`
--
ALTER TABLE `skierclubseason`
  ADD CONSTRAINT `FK_SCSClub` FOREIGN KEY (`clubId`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `FK_SCSSkier` FOREIGN KEY (`userName`) REFERENCES `skier` (`userName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
