<?php
	include_once("classes.php");
	class DOMModel {
		protected
			$xml = null;
			
		public function __construct($xml = null) {
			if ($xml) {
				$this->xml = new DOMDocument;
				$this->xml->load("$xml");
			} 
			else {
				//Create DOM connection
				$this->xml = new DOMDocument;
				$this->xml->load('SkierLogs.xml');
				}
		}
		
		public function getSkiers() {
			$skierList = array();
			$xpath = new DOMXpath($this->xml);
			$script = $xpath->query("//SkierLogs/Skiers/Skier");
			foreach ($script as $skier) {
				$userName = $skier->getAttribute('userName');
				
				$firstName = $skier->getElementsByTagName("FirstName");
				$valueFirstName = $firstName->item(0)->nodeValue;
				
				$lastName = $skier->getElementsByTagName("LastName");
				$valueLastName = $lastName->item(0)->nodeValue;
				
				$yearOfBirth = $skier->getElementsByTagName("YearOfBirth");
				$valueYearOfBirth = $yearOfBirth->item(0)->nodeValue;
			
				$skierList[] = new Skier($userName, $valueFirstName, $valueLastName, $valueYearOfBirth);
			}
			return $skierList;
		}
		
		public function getClubs() {
			$clubList = array();
			$xpath = new DOMXpath($this->xml);
			$script = $xpath->query("//SkierLogs/Clubs/Club");
			foreach ($script as $club) {
				$id = $club->getAttribute('id');
				
				$name = $club->getElementsByTagName("Name");
				$valueName = $name->item(0)->nodeValue;
				
				$city = $club->getElementsByTagName("City");
				$valueCity = $city->item(0)->nodeValue;
				
				$county = $club->getElementsByTagName("County");
				$valueCounty = $county->item(0)->nodeValue;
			
				$clubList[] = new Club($id, $valueName, $valueCity, $valueCounty);
			}
			return $clubList;
		}
		
		public function getSeasons() {
			$seasonList = array();
			$xpath = new DOMXpath($this->xml);
			$script = $xpath->query("//SkierLogs/Season");
			foreach ($script as $season) {
				$fallYear = $season->getAttribute('fallYear');
				
				foreach($season->getElementsByTagName('Skiers') as $skiers) {
					$clubId = $skiers->getAttribute('clubId');
					if ($clubId == "") $clubId = null; 
					
					foreach($skiers->getElementsByTagName('Skier') as $skier) {
						$userName = $skier->getAttribute('userName');
						$totalDistance = 0;
						
						foreach($skier->getElementsByTagName('Distance') as $entry) {
							$totalDistance += $entry->nodeValue;
						}
						$seasonList[] = new Season($userName, $fallYear, $clubId, $totalDistance);
					}
				}
			}
			return $seasonList;
		}
			
		
	}
			
	class DBModel {
		protected 
			$db = null;
		
		 public function __construct($db = null)  
		{  
	    if ($db) {
			$this->db = $db;
		}
		else {
			// Create PDO connection
			try {
				$this->db = new PDO('mysql:host=localhost;dbname=assignment5;', 'root', ''); 
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				}
				catch(PDOEXCEPTION $pdoe) {
				echo $pdoe->getMessage();
				}
			}
		
		}
		
		public function addSkiers($skiers) {
			try {
				foreach($skiers as $skier) { 
				$stmt = $this->db->prepare("INSERT INTO skier (userName, firstName, lastName, yearOfBirth) 
				VALUES(?, ?, ?, ?)");
				$stmt->execute(array($skier->userName, $skier->firstName, $skier->lastName, $skier->yearOfBirth));
				}
				
				}
				catch(PDOEXCEPTION $pdoe) {
					echo "Unable to add to skiers to database";
					echo $pdoe->getMessage(); 
			} 
		}
		
		
		public function addClubs($clubs) {
			try {
				foreach($clubs as $club) { 
				$stmt = $this->db->prepare("INSERT INTO club (id, clubName, cityName, county) 
				VALUES(?, ?, ?, ?)");
				$stmt->execute(array($club->id, $club->name, $club->city, $club->county));
				}
				
				}
				catch(PDOEXCEPTION $pdoe) {
					echo "unable to add clubs to database";
					echo $pdoe->getMessage(); 
			} 
		}
		
		public function addSeasons($seasons) {
			try {
				foreach($seasons as $season) { 
				$stmt = $this->db->prepare("INSERT INTO skierclubseason (userName, fallYear, clubId, totalDistance) 
				VALUES(?, ?, ?, ?)");
				$stmt->execute(array($season->userName, $season->fallYear, $season->clubId, $season->totalDistance));
				}
				
				}
				catch(PDOEXCEPTION $pdoe) {
					echo "unable to add seasons to database";
					echo $pdoe->getMessage(); 
			} 
		}
		
	}
	
?>