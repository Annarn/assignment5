<?php
	include_once("model/model.php");
	include_once("model/classes.php");
	include_once("view/view.php");
	
	class Controller {
		public $db, $xml, $view;
		
		public function __construct()  {  
        $this->db = new DBModel();
		$this->xml = new DOMModel();
		$this->view = new View();
		} 	
		
		public function invoke() {
		$skiers = $this->xml->getSkiers();
		$this->db->addSkiers($skiers);
		$clubs = $this->xml->getClubs();
		$this->db->addClubs($clubs);
		$seasons = $this->xml->getSeasons();
		$this->db->addSeasons($seasons);
		$this->view->create();
		}
	}

?>